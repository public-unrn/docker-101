# Telegram Bot con langchain
[fuente](https://medium.com/@obaff/building-a-telegram-bot-using-langchain-openai-and-the-telegram-api-1834167e524b)

## Botfather
[BotFather](https://telegram.me/BotFather)
## Google API Gemini
[aistudio.google](https://aistudio.google.com/u/0/apikey)

# Docker Run

```bash
docker run --name  telegram \
-e TELEGRAM_BOT_TOKEN=..... \
-e GOOGLE_API_KEY=......... \
registry.gitlab.com/public-unrn/docker-101/telegram:2.0
```

# Docker Compose

```bash 
docker-compose up
```