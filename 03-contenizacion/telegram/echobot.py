import os
import logging
from telegram import Update
from telegram.ext import Application, CommandHandler, MessageHandler, ContextTypes, filters
import google.generativeai as genai
from langchain_google_genai import ChatGoogleGenerativeAI
from langchain_core.prompts import ChatPromptTemplate

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# Set up Google Gemini API key - Verificamos primero si existe en el entorno
api_key = os.getenv('GOOGLE_API_KEY')
if not api_key:
    raise ValueError("GOOGLE_API_KEY environment variable is not set.")

# Configurar la API de Google con la clave
genai.configure(api_key=api_key)

# Define el modelo y prompt usando el enfoque moderno de LangChain
prompt = ChatPromptTemplate.from_template("""
Eres un asistente virtual conversacional en español, diseñado para ser útil, amable y preciso.

CONTEXTO:
- El usuario habla español.
- Debes proporcionar respuestas claras, concisas y útiles.
- Si no sabes la respuesta, admítelo honestamente en lugar de inventar información.

INSTRUCCIONES:
1. Mantén un tono amable y profesional en todo momento.
2. Adapta tu nivel de detalle a la complejidad de la pregunta.
3. Ofrece ejemplos cuando sea apropiado para ilustrar tus puntos.
4. Cuando sea relevante, sugiere preguntas de seguimiento.
5. Si la pregunta es ambigua, solicita aclaraciones.

La consulta del usuario es: {question}

Responde de manera útil, completa y en español.
""")
llm = ChatGoogleGenerativeAI(model="gemini-2.0-flash", temperature=0.7, google_api_key=api_key)

# Crear la cadena usando el nuevo método de composición de LangChain
chain = prompt | llm

# Start the bot
async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    await update.message.reply_markdown_v2(
        fr'Hi {user.mention_markdown_v2()}\! I\'m a bot powered by Google Gemini\. Ask me anything\.'
    )

# Help command
async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /help is issued."""
    await update.message.reply_text('Ask me any question, and I\'ll try to answer using Gemini AI!')

# Handle messages
async def handle_message(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Handle user messages and generate responses using LangChain."""
    user_message = update.message.text
    try:
        # Generate a response using the modern LangChain pipeline
        response = chain.invoke({"question": user_message})
        # Extraer el contenido del mensaje del modelo
        response_text = response.content
        await update.message.reply_text(response_text)
    except Exception as e:
        logger.error(f"Error: {e}")
        await update.message.reply_text("Sorry, I couldn't process your request at the moment.")

# Error handler
async def error_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Log Errors caused by Updates."""
    logger.warning(f'Update "{update}" caused error "{context.error}"')

def main() -> None:
    """Start the bot."""
    # Your Telegram bot token from BotFather
    token = os.getenv('TELEGRAM_BOT_TOKEN')
    if not token:
        raise ValueError("TELEGRAM_BOT_TOKEN environment variable is not set.")

    # Create the Application and pass it your bot's token
    application = Application.builder().token(token).build()

    # Add command handlers
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("help", help_command))

    # Add message handler
    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, handle_message))

    # Add error handler
    application.add_error_handler(error_handler)

    # Start the Bot
    application.run_polling()

if __name__ == '__main__':
    main()